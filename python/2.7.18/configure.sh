#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX \
	--enable-optimizations
