#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX           \
	--sysconfdir=$PREFIX       \
	--enable-x11-backend
