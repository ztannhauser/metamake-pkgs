#! /bin/sh
set -ev
CFLAGS="-fPIE -O2" ../src/configure \
	--prefix=$PREFIX \
	--enable-add-ons \
	--enable-kernel=5.0.1 \
	--with-headers=$METAMAKE_PKGS/linux-kernel-headers/5.0.1/include
