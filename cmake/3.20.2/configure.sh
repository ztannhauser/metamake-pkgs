set -ev
../src/configure \
	--prefix=$PREFIX \
	--no-system-libs \
	--system-libuv
