#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX \
	--disable-multilib
