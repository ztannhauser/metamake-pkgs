#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX \
	--disable-all-programs \
	--enable-libblkid \
	--enable-libmount
