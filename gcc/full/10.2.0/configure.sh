#! /bin/sh
set -ev
../src/configure \
    --prefix=$PREFIX \
    --with-gmp=$PACKAGES/gmp/6.2.1/prefix \
    --with-mpfr=$PACKAGES/mpfr/4.0.2/prefix \
    --with-mpc=$PACKAGES/mpc/1.1.0/prefix \
    --disable-multilib \
    --with-system-zlib \
    --enable-languages=c,c++,d,fortran,go,objc,obj-c++
