#! /bin/sh
set -ev
../src/configure \
    --prefix=$PREFIX \
    --with-gmp=$PACKAGES/gmp/6.2.1/prefix \
    --enable-thread-safe
