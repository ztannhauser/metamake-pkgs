#! /bin/sh
set -ev
../src/configure                      \
	--prefix=$PREFIX                  \
	--enable-utf8                     \
	--enable-unicode-properties
