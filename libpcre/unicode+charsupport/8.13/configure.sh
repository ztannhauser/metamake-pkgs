#! /bin/sh
set -ev
../src/configure                      \
	--prefix=$PREFIX                  \
	--enable-utf8                     \
	--enable-unicode-properties       \
	--enable-pcre16                   \
	--enable-pcre32
