

prepend-path C_INCLUDE_PATH		indir(`$PREFIX')/lib/libffi-3.2.1/include
prepend-path CPLUS_INCLUDE_PATH	indir(`$PREFIX')/lib/libffi-3.2.1/include

prepend-path LIBRARY_PATH		indir(`$PREFIX')/lib
prepend-path LD_LIBRARY_PATH	indir(`$PREFIX')/lib

prepend-path MANPATH			indir(`$PREFIX')/share/man

prepend-path PKG_CONFIG_PATH	indir(`$PREFIX')/lib/pkgconfig


