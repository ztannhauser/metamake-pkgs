#! /bin/sh
set -ev
../src/autogen.sh \
	--prefix=$PREFIX \
	--with-pcre=internal
