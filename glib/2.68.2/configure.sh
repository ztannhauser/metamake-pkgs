#! /bin/sh
set -ev
meson \
	--reconfigure \
	--wrap-mode=nofallback \
	--prefix=$PREFIX   \
	-Dman=false        \
	-Dselinux=disabled \
	../src
