#! /bin/sh
set -ev
CFLAGS=-Wno-format-overflow ../src/configure \
	--prefix=$PREFIX \
	--with-pcre=internal
