#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX \
	--without-python \
	--with-history
