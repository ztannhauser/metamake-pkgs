#! /bin/sh
set -ev
../src/configure \
	--prefix=$PREFIX \
	--enable-static \
	--enable-shared \
	--without-python
