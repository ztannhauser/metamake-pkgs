#! /bin/bash
set -ev
sed -i s/3000/5000/ libxslt/transform.c doc/xsltproc.{1,xml}
