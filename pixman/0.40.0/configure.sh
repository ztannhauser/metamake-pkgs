#! /bin/sh
set -ev
meson \
	--prefix=$PREFIX \
	--wrap-mode=nofallback \
	../src/
