#! /bin/bash
set -ev
AL_OPTS="-I $PACKAGES/autoconf/2.69/prefix/share/autoconf/autoconf "
AL_OPTS+="-I $PACKAGES/autoconf/2.69/prefix/share/autoconf"
export AL_OPTS
./autogen.sh
